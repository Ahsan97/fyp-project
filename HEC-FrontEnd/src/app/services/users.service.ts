import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()

export class UsersService {
  constructor(private http: HttpClient, private router: Router) { }

  registerNewUser(userData): Observable<any> {
    return this.http.post('http://127.0.0.1:8000/api/register/', userData);
  }

  loginUser(data): Observable<any> {
    return this.http.post('http://127.0.0.1:8000/api/login/', data);
  }



  logout() {
    this.router.navigate(['/login'])
    localStorage.removeItem('token');
    localStorage.removeItem('user');

  }

  userRole(): Observable<any> {
    const httpoptions = {
      headers: new HttpHeaders({ 'Authorization': 'token ' + localStorage.getItem('token') })
    };
    return this.http.get('http://127.0.0.1:8000/api/role/', httpoptions);
  }

  paperPresentationForm(data) {
    const httpoptions = {
      headers: new HttpHeaders({ 'Authorization': 'token ' + localStorage.getItem('token') })
    };
    return this.http.post('http://127.0.0.1:8000/api/paper-presenting-form/', data, httpoptions);
  }

  getPaperPresentationList(): Observable<any> {
    const httpoptions = {
      headers: new HttpHeaders({ 'Authorization': 'token ' + localStorage.getItem('token') })
    };
    return this.http.get('http://127.0.0.1:8000/api/paper-presenting-form/', httpoptions);
  }

  getPaperPresentationFormList(formid): Observable<any> {
    const httpoptions = {
      headers: new HttpHeaders({ 'Authorization': 'token ' + localStorage.getItem('token') })
    };
    return this.http.get('http://127.0.0.1:8000/api/paper-presenting-form/' + formid + '/', httpoptions);
  }

  oralPaperPresentationDocuments(data): Observable<any> {
    const httpoptions = {
      headers: new HttpHeaders({ 'Authorization': 'token ' + localStorage.getItem('token') })
    };
    return this.http.post('http://127.0.0.1:8000/api/paper-presentation-oral-documents/', data, httpoptions);
  }
  getpaperpresentationoralDocumentsList(formid): Observable<any> {
    const httpoptions = {
      headers: new HttpHeaders({ 'Authorization': 'token ' + localStorage.getItem('token') })
    };
    return this.http.get('http://127.0.0.1:8000/api/paper-presentation-oral-documents/' + formid + '/', httpoptions);
  }

  posterPaperPresentationDocuments(data): Observable<any> {
    const httpoptions = {
      headers: new HttpHeaders({ 'Authorization': 'token ' + localStorage.getItem('token') })
    };
    return this.http.post('http://127.0.0.1:8000/api/paper-presentation-poster-documents/', data, httpoptions);
  }

  getpaperpresentationposterDocumentsList(formid): Observable<any> {
    const httpoptions = {
      headers: new HttpHeaders({ 'Authorization': 'token ' + localStorage.getItem('token') })
    };
    return this.http.get('http://127.0.0.1:8000/api/paper-presentation-poster-documents/' + formid + '/', httpoptions);
  }

  higherStudiesForm(data) {
    const httpoptions = {
      headers: new HttpHeaders({ 'Authorization': 'token ' + localStorage.getItem('token') })
    };
    return this.http.post('http://127.0.0.1:8000/api/higher-studies-form/', data, httpoptions);
  }

  gethigherStudiesList(): Observable<any> {
    const httpoptions = {
      headers: new HttpHeaders({ 'Authorization': 'token ' + localStorage.getItem('token') })
    };
    return this.http.get('http://127.0.0.1:8000/api/higher-studies-form/', httpoptions);
  }

  gethigherStudiesFormList(formid): Observable<any> {
    const httpoptions = {
      headers: new HttpHeaders({ 'Authorization': 'token ' + localStorage.getItem('token') })
    };
    return this.http.get('http://127.0.0.1:8000/api/higher-studies-form/' + formid + '/', httpoptions);
  }

  higherStudiesphdDocuments(data) {
    const httpoptions = {
      headers: new HttpHeaders({ 'Authorization': 'token ' + localStorage.getItem('token') })
    };
    return this.http.post('http://127.0.0.1:8000/api/higher-studies-phd-documents/', data, httpoptions);
  }

  gethigherStudiesphdDocumentsList(formid): Observable<any> {
    const httpoptions = {
      headers: new HttpHeaders({ 'Authorization': 'token ' + localStorage.getItem('token') })
    };
    return this.http.get('http://127.0.0.1:8000/api/higher-studies-phd-documents/' + formid + '/', httpoptions);
  }

  higherStudiespostdoctorateDocuments(data) {
    const httpoptions = {
      headers: new HttpHeaders({ 'Authorization': 'token ' + localStorage.getItem('token') })
    };
    return this.http.post('http://127.0.0.1:8000/api/higher-studies-postdoctorate-documents/', data, httpoptions)
  }
  gethigherStudiespostdoctorateDocumentsList(formid): Observable<any> {
    const httpoptions = {
      headers: new HttpHeaders({ 'Authorization': 'token ' + localStorage.getItem('token') })
    };
    return this.http.get('http://127.0.0.1:8000/api/higher-studies-postdoctorate-documents/' + formid + '/', httpoptions);
  }

  emailApplicant(data): Observable<any> {
    return this.http.post('http://127.0.0.1:8000/api/email-user/', data);
  }

  resetPassword(data): Observable<any> {
    return this.http.post('http://127.0.0.1:8000/api/password-reset/', data);
  }
}

