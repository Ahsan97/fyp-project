import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-higherstudies-postdoctorate-documents',
  templateUrl: './admin-higherstudies-postdoctorate-documents.component.html',
  styleUrls: ['./admin-higherstudies-postdoctorate-documents.component.scss'],
  providers: [UsersService]
})
export class AdminHigherstudiesPostdoctorateDocumentsComponent implements OnInit {
  formData: any[];
  constructor(private userService: UsersService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    let formId = this.activatedRoute.snapshot.paramMap.get('formId');
    this.userService.gethigherStudiespostdoctorateDocumentsList(formId).subscribe(
      response => {
        this.formData = response;
        console.log(response);
      }
    );
  }
  logout() {
    return this.userService.logout();
  }
  response() {
    this.router.navigate(['/admin-email-applicant']);
  }

}
