import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHigherstudiesPostdoctorateDocumentsComponent } from './admin-higherstudies-postdoctorate-documents.component';

describe('AdminHigherstudiesPostdoctorateDocumentsComponent', () => {
  let component: AdminHigherstudiesPostdoctorateDocumentsComponent;
  let fixture: ComponentFixture<AdminHigherstudiesPostdoctorateDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminHigherstudiesPostdoctorateDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHigherstudiesPostdoctorateDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
