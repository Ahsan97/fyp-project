import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HigherstudiespostdoctoratedocumentsComponent } from './higherstudiespostdoctoratedocuments.component';

describe('HigherstudiespostdoctoratedocumentsComponent', () => {
  let component: HigherstudiespostdoctoratedocumentsComponent;
  let fixture: ComponentFixture<HigherstudiespostdoctoratedocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HigherstudiespostdoctoratedocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HigherstudiespostdoctoratedocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
