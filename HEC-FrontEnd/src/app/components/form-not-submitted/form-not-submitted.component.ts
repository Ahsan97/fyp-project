import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form-not-submitted',
  templateUrl: './form-not-submitted.component.html',
  styleUrls: ['./form-not-submitted.component.scss']
})
export class FormNotSubmittedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
