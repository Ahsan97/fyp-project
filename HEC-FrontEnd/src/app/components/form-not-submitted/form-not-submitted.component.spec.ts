import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNotSubmittedComponent } from './form-not-submitted.component';

describe('FormNotSubmittedComponent', () => {
  let component: FormNotSubmittedComponent;
  let fixture: ComponentFixture<FormNotSubmittedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormNotSubmittedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormNotSubmittedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
