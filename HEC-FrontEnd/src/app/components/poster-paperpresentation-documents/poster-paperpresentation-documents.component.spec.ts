import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosterPaperpresentationDocumentsComponent } from './poster-paperpresentation-documents.component';

describe('PosterPaperpresentationDocumentsComponent', () => {
  let component: PosterPaperpresentationDocumentsComponent;
  let fixture: ComponentFixture<PosterPaperpresentationDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosterPaperpresentationDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosterPaperpresentationDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
