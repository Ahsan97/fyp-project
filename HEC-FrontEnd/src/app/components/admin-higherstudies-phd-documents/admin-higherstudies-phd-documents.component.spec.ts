import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHigherstudiesDocumentsComponent } from './admin-higherstudies-phd-documents.component';

describe('AdminHigherstudiesDocumentsComponent', () => {
  let component: AdminHigherstudiesDocumentsComponent;
  let fixture: ComponentFixture<AdminHigherstudiesDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminHigherstudiesDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHigherstudiesDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
