import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHigherStudiesComponent } from './admin-higher-studies.component';

describe('AdminHigherStudiesComponent', () => {
  let component: AdminHigherStudiesComponent;
  let fixture: ComponentFixture<AdminHigherStudiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminHigherStudiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHigherStudiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
