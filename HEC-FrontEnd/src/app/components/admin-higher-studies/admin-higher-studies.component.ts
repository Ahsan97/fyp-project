import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'admin-higher-studies',
  templateUrl: './admin-higher-studies.component.html',
  styleUrls: ['./admin-higher-studies.component.scss'],
  providers: [UsersService]
})
export class AdminHigherStudiesComponent {

  lists: any[];

  constructor(private userService: UsersService, private router: Router) {
    this.userService.gethigherStudiesList().subscribe(
      response => {
        this.lists = response;
      }
    )
  }

  logout() {
    return this.userService.logout();
  }
  gotoDetail(i) {
    let path = '/admin-higherstudies-form/' + this.lists[i].id;
    this.router.navigate([path]);
  }
}
