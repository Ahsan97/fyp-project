import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  providers: [UsersService]
})
export class ForgotPasswordComponent implements OnInit {
  input;
  emailpattern = "[a-z0 - 9!#$%& '*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&' * +/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
  errorMessage: string = '';
  successMessage: string = '';

  constructor(private userService: UsersService) { }

  ngOnInit() {
    this.input = {
      email: ''
    };
  }

  submitEmail() {
    this.successMessage = '';
    this.errorMessage = '';
    this.userService.resetPassword(this.input).subscribe(
      response => {
        this.successMessage = 'Please Check you mail for password reset link!';
        console.log(response)
      },
      error => this.errorMessage = 'Please enter correct email or try again later!'
    );
  }
}
