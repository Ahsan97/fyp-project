import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HigherstudiesdocumentsComponent } from './higherstudiesdocuments.component';

describe('HigherstudiesdocumentsComponent', () => {
  let component: HigherstudiesdocumentsComponent;
  let fixture: ComponentFixture<HigherstudiesdocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HigherstudiesdocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HigherstudiesdocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
