import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { FormNotSubmittedComponent } from '../form-not-submitted/form-not-submitted.component';
import { FormSubmittedComponent } from '../form-submitted/form-submitted.component';

@Component({
  selector: 'higherstudiesdocuments',
  templateUrl: './higherstudiesdocuments.component.html',
  styleUrls: ['./higherstudiesdocuments.component.scss'],
  providers: [UsersService]
})
export class HigherstudiesdocumentsComponent implements OnInit {

  selectedFiles: { [key: string]: File } = {};
  constructor(private userService: UsersService, private dialog: MatDialog) { }

  ngOnInit() {

  }
  onFileSelected(event, documentKey: string) {
    const file = <File>event.target.files[0];
    this.selectedFiles[documentKey] = file;
  }
  onSubmit() {
    const formData = new FormData();

    Object.keys(this.selectedFiles).forEach(documentKey => {
      let file = this.selectedFiles[documentKey];
      formData.append(documentKey, file, file.name);
    });
    this.userService.higherStudiesphdDocuments(formData).subscribe(
      response => {
        console.log(response);
        this.dialog.open(FormSubmittedComponent);
      },
      error => {
        this.dialog.open(FormNotSubmittedComponent);
      }
    );

  }

  logout() {
    return this.userService.logout();
  }

}
