import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'home-student',
  templateUrl: './home-student.component.html',
  styleUrls: ['./home-student.component.scss'],
  providers: [UsersService]
})
export class HomeStudentComponent implements OnInit {

  constructor(private userService: UsersService) { }

  ngOnInit() {
  }
  logout() {
    return this.userService.logout();
  }

}
