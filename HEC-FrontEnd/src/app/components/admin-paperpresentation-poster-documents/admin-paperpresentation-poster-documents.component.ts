import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-paperpresentation-poster-documents',
  templateUrl: './admin-paperpresentation-poster-documents.component.html',
  styleUrls: ['./admin-paperpresentation-poster-documents.component.scss'],
  providers: [UsersService]
})
export class AdminPaperpresentationPosterDocumentsComponent implements OnInit {
  formData: any[];
  constructor(private userService: UsersService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    let formId = this.activatedRoute.snapshot.paramMap.get('formId');
    this.userService.getpaperpresentationposterDocumentsList(formId).subscribe(
      response => {
        this.formData = response;
        console.log(response);
      }
    );
  }
  logout() {
    return this.userService.logout();
  }

  response() {
    this.router.navigate(['/admin-email-applicant']);
  }
}