import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPaperpresentationPosterDocumentsComponent } from './admin-paperpresentation-poster-documents.component';

describe('AdminPaperpresentationPosterDocumentsComponent', () => {
  let component: AdminPaperpresentationPosterDocumentsComponent;
  let fixture: ComponentFixture<AdminPaperpresentationPosterDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPaperpresentationPosterDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPaperpresentationPosterDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
