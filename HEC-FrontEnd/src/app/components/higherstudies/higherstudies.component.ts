import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'higherstudies',
  templateUrl: './higherstudies.component.html',
  styleUrls: ['./higherstudies.component.scss'],
  providers: [UsersService]
})
export class HigherstudiesComponent implements OnInit {
  namepattern = "^([a-zA-Z]{2,}\\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\\s?([a-zA-Z]{1,})?)";
  cnicpattern = "^[0-9]{5}-[0-9]{7}-[0-9]$";
  emailpattern = "[a-z0 - 9!#$%& '*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&' * +/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
  input;

  constructor(private router: Router, private userService: UsersService) { }

  ngOnInit() {
    this.input = {
      name: '',
      cnic: '',
      highest_qualification: '',
      designation: '',
      department: '',
      university: '',
      address: '',
      cellphone: '',
      email: '',
      travel_purpose: '',
      country_name: '',
      university_name: '',
      department_name: '',
      program_name: '',
      research_title: '',
      registration_date: '',
      joining_date: '',
      stay_duration: '',
      return_date: '',
      travel_cost: '',
      travel_sponsor: '',
      tuition_fee: '',
      tuition_fee_sponsor: '',
      accommodation_cost: '',
      accommodation_sponsor: '',
      other_cost: '',
      other_cost_sponsor: '',
      hec_travel_cost: ''
    };
  }
  onSubmit() {
    this.userService.higherStudiesForm(this.input).subscribe(
      response => {

        if (this.input.travel_purpose == 'Application for Ph.D.') {
          this.router.navigate(['/higher-studies-phd-documents']);
        } else {
          this.router.navigate(['/higher-studies-postdoctorate-documents']);
        }

        console.log(response);
      }
    )
  }

  logout() {
    return this.userService.logout();
  }
}
