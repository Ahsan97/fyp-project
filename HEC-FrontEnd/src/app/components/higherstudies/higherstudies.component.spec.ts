import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HigherstudiesComponent } from './higherstudies.component';

describe('HigherstudiesComponent', () => {
  let component: HigherstudiesComponent;
  let fixture: ComponentFixture<HigherstudiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HigherstudiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HigherstudiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
