import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OralPaperpresentationDocumentsComponent } from './oral-paperpresentation-documents.component';

describe('OralPaperpresentationDocumentsComponent', () => {
  let component: OralPaperpresentationDocumentsComponent;
  let fixture: ComponentFixture<OralPaperpresentationDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OralPaperpresentationDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OralPaperpresentationDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
