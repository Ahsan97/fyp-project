import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'adminpaperform',
  templateUrl: './adminpaperform.component.html',
  styleUrls: ['./adminpaperform.component.scss'],
  providers: [UsersService]
})
export class AdminpaperformComponent implements OnInit {

  formData: any;
  constructor(private userService: UsersService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    let formId = this.activatedRoute.snapshot.paramMap.get('formId');
    this.userService.getPaperPresentationFormList(formId).subscribe(
      response => {
        this.formData = response;
        console.log(response);
        localStorage.setItem('email', this.formData.email);
      }
    );
  }

  logout() {
    return this.userService.logout();
  }
  Back() {
    this.router.navigate(['/admin-paper']);
  }
  onSubmit(formData) {
    if (formData.presentation_mode == 'Oral Presentation') {
      let path = '/admin-paperpresention-oral-documents/' + formData.id;
      this.router.navigate([path]);
    }
    else if (formData.presentation_mode == 'Poster Presentation') {
      let path = '/admin-paperpresention-poster-documents/' + formData.id;
      this.router.navigate([path]);
    }
  }
}
