import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminpaperformComponent } from './adminpaperform.component';

describe('AdminpaperformComponent', () => {
  let component: AdminpaperformComponent;
  let fixture: ComponentFixture<AdminpaperformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminpaperformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminpaperformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
