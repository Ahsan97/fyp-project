import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'admin-paper-presentation',
  templateUrl: './admin-paper-presentation.component.html',
  styleUrls: ['./admin-paper-presentation.component.scss'],
  providers: [UsersService]
})
export class AdminPaperPresentationComponent {

  lists: any[];

  constructor(private userService: UsersService, private router: Router) {
    this.userService.getPaperPresentationList().subscribe(
      response => {
        this.lists = response;
      }
    )
  }

  logout() {
    return this.userService.logout();
  }
  gotoDetail(i) {
    let path = '/admin-paper-form/' + this.lists[i].id;
    this.router.navigate([path]);
  }
}
