import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPaperPresentationComponent } from './admin-paper-presentation.component';

describe('AdminPaperPresentationComponent', () => {
  let component: AdminPaperPresentationComponent;
  let fixture: ComponentFixture<AdminPaperPresentationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPaperPresentationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPaperPresentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
