import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHigherstudiesFormComponent } from './admin-higherstudies-form.component';

describe('AdminHigherstudiesFormComponent', () => {
  let component: AdminHigherstudiesFormComponent;
  let fixture: ComponentFixture<AdminHigherstudiesFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminHigherstudiesFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHigherstudiesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
