import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-higherstudies-form',
  templateUrl: './admin-higherstudies-form.component.html',
  styleUrls: ['./admin-higherstudies-form.component.scss'],
  providers: [UsersService]
})
export class AdminHigherstudiesFormComponent implements OnInit {
  formData: any;
  constructor(private userService: UsersService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    let formId = this.activatedRoute.snapshot.paramMap.get('formId');
    this.userService.gethigherStudiesFormList(formId).subscribe(
      response => {
        this.formData = response;
        console.log(response);
        localStorage.setItem('email', this.formData.email);
      }
    );
  }

  logout() {
    return this.userService.logout();
  }
  Back() {
    this.router.navigate(['/admin-higher-studies']);
  }

  onSubmit(formData) {
    if (formData.travel_purpose == 'Application for Ph.D.') {
      let path = '/admin-higherstudies-phd-documents/' + formData.id;
      this.router.navigate([path]);
    }
    else if (formData.travel_purpose == 'Application for Post-Doctorate') {
      let path = '/admin-higherstudies-postdoctorate-documents/' + formData.id;
      this.router.navigate([path]);
    }
  }
}
