import { Component, OnInit } from "@angular/core";
import { UsersService } from "src/app/services/users.service";

import { Router } from "@angular/router";

@Component({
  selector: "login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
  providers: [UsersService]
})
export class LoginComponent implements OnInit {
  emailpattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{3,4}$";
  input;
  errorMessage: string = "";
  successMessage: string = "";

  constructor(private userService: UsersService, private router: Router) {}

  ngOnInit() {
    this.userService.logout();
    this.input = {
      username: "",
      password: ""
    };
  }
  loginUser() {
    this.errorMessage = "";
    this.userService.loginUser(this.input).subscribe(
      response => {
        this.successMessage = "Logging you in";
        console.log(response);
        localStorage.setItem("token", response.token);

        this.userService.userRole().subscribe(response => {
          //console.log(response);
          localStorage.setItem("user", response.user_role);
          if (localStorage.getItem("user") == "applicant") {
            this.router.navigate(["/home-student"]);
          } else if (localStorage.getItem("user") == "committe") {
            this.router.navigate(["/admin-paper"]);
          }
        });
      },
      error => (this.errorMessage = "Username or password is incorrect")
    );
  }

  resolve() {
    window.location.href = "http://127.0.0.1:8000/api/password-reset/";
  }
}
