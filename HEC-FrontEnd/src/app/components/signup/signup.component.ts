import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';


@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  providers: [UsersService]
})
export class SignupComponent implements OnInit {
  register;
  emailpattern = "[a-z0 - 9!#$%& '*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&' * +/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
  successMessage: string = '';
  errorMessage: string = '';

  constructor(private userService: UsersService) {

  }

  ngOnInit() {
    this.register = {
      username: '',
      email: '',
      password: ''
    };
  }

  registerUser() {
    this.successMessage = '';
    this.errorMessage = '';
    this.userService.registerNewUser(this.register).subscribe(
      response => {
        console.log(response);
        this.successMessage = 'Your account has been created';
      },
      error => this.errorMessage = 'This email or username is already taken!'
    );
  }

}
