import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaperpresentationComponent } from './paperpresentation.component';

describe('Paperpresentation1Component', () => {
  let component: PaperpresentationComponent;
  let fixture: ComponentFixture<PaperpresentationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaperpresentationComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaperpresentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
