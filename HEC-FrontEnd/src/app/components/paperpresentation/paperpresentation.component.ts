import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'paperpresentation',
  templateUrl: './paperpresentation.component.html',
  styleUrls: ['./paperpresentation.component.scss'],
  providers: [UsersService]
})
export class PaperpresentationComponent implements OnInit {
  namepattern = "^([a-zA-Z]{2,}\\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\\s?([a-zA-Z]{1,})?)";
  cnicpattern = "^[0-9]{5}-[0-9]{7}-[0-9]$";
  emailpattern = "[a-z0 - 9!#$%& '*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&' * +/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
  urlpattern = "^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$"
  input;

  constructor(private router: Router, private userService: UsersService) { }

  ngOnInit() {
    this.input = {
      status: '',
      name: '',
      cnic: '',
      highest_qualification: '',
      designation: '',
      department: '',
      university: '',
      address: '',
      cellphone: '',
      email: '',
      event_title: '',
      event_starting_date: '',
      event_ending_date: '',
      venue: '',
      organizer: '',
      website_address: '',
      event_type: '',
      research_title: '',
      paper_status: '',
      presentation_mode: '',
      paper_review: '',
      paper_publish: '',
      travel_cost: '',
      travel_sponsor: '',
      registration_cost: '',
      registration_sponsor: '',
      accommodation_cost: '',
      accommodation_sponsor: '',
      daily_allowance_cost: '',
      daily_allowance_sponsor: '',
      hec_travel_cost: '',
      hec_registration_fee: '',
      hec_accommodation: '',
      hec_allowance: '',
      scholarship_details: '',
      travel_grant_details: ''
    };
  }
  onSubmit() {
    this.userService.paperPresentationForm(this.input).subscribe(
      response => {
        if (this.input.presentation_mode == 'Oral Presentation') {
          this.router.navigate(['/oral-paper-presenting-documents']);
        } else {
          this.router.navigate(['/poster-paper-presenting-documents']);
        }

        console.log(response);

      }
    );

  }
  logout() {
    return this.userService.logout();
  }
}
