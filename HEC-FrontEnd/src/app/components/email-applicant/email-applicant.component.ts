import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-email-applicant',
  templateUrl: './email-applicant.component.html',
  styleUrls: ['./email-applicant.component.scss']
})
export class EmailApplicantComponent implements OnInit {
  input;
  emailpattern = "[a-z0 - 9!#$%& '*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&' * +/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
  successMessage: string = '';
  errorMessage: string = '';
  constructor(private userService: UsersService, private router: Router) { }

  ngOnInit() {
    this.input = {
      email: localStorage.getItem('email'),
      subject: '',
      message: ''
    };
  }

  sendEmail() {
    this.successMessage = '';
    this.errorMessage = '';
    this.userService.emailApplicant(this.input).subscribe(
      response => {
        console.log(response);
        this.successMessage = 'Your email has been sent';
      },
      error => this.errorMessage = 'Please try again later!'

    );
  }

  logout() {
    return this.userService.logout();
  }
}
