import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailApplicantComponent } from './email-applicant.component';

describe('EmailApplicantComponent', () => {
  let component: EmailApplicantComponent;
  let fixture: ComponentFixture<EmailApplicantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailApplicantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailApplicantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
