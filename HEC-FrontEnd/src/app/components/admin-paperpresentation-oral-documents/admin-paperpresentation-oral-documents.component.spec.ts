import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPaperpresentationOralDocumentsComponent } from './admin-paperpresentation-oral-documents.component';

describe('AdminPaperpresentationOralDocumentsComponent', () => {
  let component: AdminPaperpresentationOralDocumentsComponent;
  let fixture: ComponentFixture<AdminPaperpresentationOralDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPaperpresentationOralDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPaperpresentationOralDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
