import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../app/components/login/login.component';
import { SignupComponent } from '../app/components/signup/signup.component';
import { HomeStudentComponent } from '../app/components/home-student/home-student.component';
import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/adminguard.guard';
import { PaperpresentationComponent } from '../app/components/paperpresentation/paperpresentation.component';
import { AdminPaperPresentationComponent } from '../app/components/admin-paper-presentation/admin-paper-presentation.component';
import { AdminHigherStudiesComponent } from '../app/components/admin-higher-studies/admin-higher-studies.component';
import { HigherstudiesComponent } from '../app/components/higherstudies/higherstudies.component';
import { HigherstudiesdocumentsComponent } from '../app/components/higherstudiesphddocuments/higherstudiesdocuments.component';
import { AdminpaperformComponent } from './components/adminpaperform/adminpaperform.component';
import { HigherstudiespostdoctoratedocumentsComponent } from './components/higherstudiespostdoctoratedocuments/higherstudiespostdoctoratedocuments.component';
import { AdminHigherstudiesFormComponent } from './components/admin-higherstudies-form/admin-higherstudies-form.component';
import { AdminHigherstudiesDocumentsComponent } from './components/admin-higherstudies-phd-documents/admin-higherstudies-phd-documents.component';
import { EmailApplicantComponent } from './components/email-applicant/email-applicant.component';
import { OralPaperpresentationDocumentsComponent } from './components/oral-paperpresentation-documents/oral-paperpresentation-documents.component';
import { PosterPaperpresentationDocumentsComponent } from './components/poster-paperpresentation-documents/poster-paperpresentation-documents.component';
import { AdminHigherstudiesPostdoctorateDocumentsComponent } from './components/admin-higherstudies-postdoctorate-documents/admin-higherstudies-postdoctorate-documents.component';
import { AdminPaperpresentationOralDocumentsComponent } from './components/admin-paperpresentation-oral-documents/admin-paperpresentation-oral-documents.component';
import { AdminPaperpresentationPosterDocumentsComponent } from './components/admin-paperpresentation-poster-documents/admin-paperpresentation-poster-documents.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent
  },
  {
    path: 'home-student',
    canActivate: [AuthGuard],
    component: HomeStudentComponent
  },
  {
    path: 'paper-presenting-form',
    canActivate: [AuthGuard],
    component: PaperpresentationComponent
  },
  {
    path: 'oral-paper-presenting-documents',
    canActivate: [AuthGuard],
    component: OralPaperpresentationDocumentsComponent
  },
  {
    path: 'poster-paper-presenting-documents',
    canActivate: [AuthGuard],
    component: PosterPaperpresentationDocumentsComponent
  },
  {
    path: 'higher-studies-form',
    canActivate: [AuthGuard],
    component: HigherstudiesComponent
  },
  {
    path: 'higher-studies-phd-documents',
    canActivate: [AuthGuard],
    component: HigherstudiesdocumentsComponent
  },
  {
    path: 'higher-studies-postdoctorate-documents',
    canActivate: [AuthGuard],
    component: HigherstudiespostdoctoratedocumentsComponent
  },
  {
    path: 'admin-paper',
    canActivate: [AuthGuard, AdminGuard],
    component: AdminPaperPresentationComponent
  },
  {
    path: 'admin-higher-studies',
    canActivate: [AuthGuard, AdminGuard],
    component: AdminHigherStudiesComponent
  },
  {
    path: 'admin-paper-form/:formId',
    canActivate: [AuthGuard, AdminGuard],
    component: AdminpaperformComponent
  },
  {
    path: 'admin-paperpresention-oral-documents/:formId',
    canActivate: [AuthGuard, AdminGuard],
    component: AdminPaperpresentationOralDocumentsComponent
  },
  {
    path: 'admin-paperpresention-poster-documents/:formId',
    canActivate: [AuthGuard, AdminGuard],
    component: AdminPaperpresentationPosterDocumentsComponent
  },
  {
    path: 'admin-higherstudies-form/:formId',
    canActivate: [AuthGuard, AdminGuard],
    component: AdminHigherstudiesFormComponent
  },
  {
    path: 'admin-higherstudies-phd-documents/:formId',
    canActivate: [AuthGuard, AdminGuard],
    component: AdminHigherstudiesDocumentsComponent
  },
  {
    path: 'admin-higherstudies-postdoctorate-documents/:formId',
    canActivate: [AuthGuard, AdminGuard],
    component: AdminHigherstudiesPostdoctorateDocumentsComponent
  },
  {
    path: 'admin-email-applicant',
    canActivate: [AuthGuard, AdminGuard],
    component: EmailApplicantComponent
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
