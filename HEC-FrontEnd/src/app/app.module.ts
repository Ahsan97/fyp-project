import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from '../app/components/navbar/navbar.component';
import { SignupComponent } from '../app/components/signup/signup.component';
import { LoginComponent } from '../app/components/login/login.component';
import { ConfirmEqualValidatorDirective } from './confirm-equal-validator.directive';
import { HomeStudentComponent } from '../app/components/home-student/home-student.component';
import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/adminguard.guard';
import { UsersService } from './services/users.service';
import { PaperpresentationComponent } from '../app/components/paperpresentation/paperpresentation.component';
import { AdminPaperPresentationComponent } from '../app/components/admin-paper-presentation/admin-paper-presentation.component';
import { AdminHigherStudiesComponent } from '../app/components/admin-higher-studies/admin-higher-studies.component';
import { HigherstudiesComponent } from '../app/components/higherstudies/higherstudies.component';
import { HigherstudiesdocumentsComponent } from '../app/components/higherstudiesphddocuments/higherstudiesdocuments.component';
import { AdminpaperformComponent } from '../app/components/adminpaperform/adminpaperform.component';
import { HigherstudiespostdoctoratedocumentsComponent } from './components/higherstudiespostdoctoratedocuments/higherstudiespostdoctoratedocuments.component';
import { AdminHigherstudiesFormComponent } from './components/admin-higherstudies-form/admin-higherstudies-form.component';
import { AdminHigherstudiesDocumentsComponent } from './components/admin-higherstudies-phd-documents/admin-higherstudies-phd-documents.component';
import { EmailApplicantComponent } from './components/email-applicant/email-applicant.component';
import { OralPaperpresentationDocumentsComponent } from './components/oral-paperpresentation-documents/oral-paperpresentation-documents.component';
import { PosterPaperpresentationDocumentsComponent } from './components/poster-paperpresentation-documents/poster-paperpresentation-documents.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormSubmittedComponent } from './components/form-submitted/form-submitted.component';
import { FormNotSubmittedComponent } from './components/form-not-submitted/form-not-submitted.component';
import { AdminHigherstudiesPostdoctorateDocumentsComponent } from './components/admin-higherstudies-postdoctorate-documents/admin-higherstudies-postdoctorate-documents.component';
import { AdminPaperpresentationOralDocumentsComponent } from './components/admin-paperpresentation-oral-documents/admin-paperpresentation-oral-documents.component';
import { AdminPaperpresentationPosterDocumentsComponent } from './components/admin-paperpresentation-poster-documents/admin-paperpresentation-poster-documents.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SignupComponent,
    LoginComponent,
    ConfirmEqualValidatorDirective,
    HomeStudentComponent,
    PaperpresentationComponent,
    AdminPaperPresentationComponent,
    AdminHigherStudiesComponent,
    HigherstudiesComponent,
    HigherstudiesdocumentsComponent,
    AdminpaperformComponent,
    HigherstudiespostdoctoratedocumentsComponent,
    AdminHigherstudiesFormComponent,
    AdminHigherstudiesDocumentsComponent,
    EmailApplicantComponent,
    OralPaperpresentationDocumentsComponent,
    PosterPaperpresentationDocumentsComponent,
    FormSubmittedComponent,
    FormNotSubmittedComponent,
    AdminHigherstudiesPostdoctorateDocumentsComponent,
    AdminPaperpresentationOralDocumentsComponent,
    AdminPaperpresentationPosterDocumentsComponent,
    ForgotPasswordComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    MatDialogModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthGuard,
    AdminGuard,
    UsersService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    FormSubmittedComponent,
    FormNotSubmittedComponent]
})
export class AppModule { }
