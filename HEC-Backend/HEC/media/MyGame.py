import arcade
class MyGame(arcade.Window):

	def __init__(self, bwidth, bheight, title,dim,players):

        	# Call the parent class's init function
		super().__init__(bwidth*dim, bheight*dim, title)

        	# Set the background color
		arcade.set_background_color(arcade.color.BLUE)
		self.bheight = bheight
		self.bwidth = bwidth
		self.players = players
		self.dim = dim
		self.sprites = arcade.SpriteList()
		self.turn = 1
		self.gameOver = False
		self.Grid = [[0 for x in range(bwidth)] for y in range(bheight)]
    
	def setup(self):
		self.sprites = arcade.SpriteList()

	def vertical_win(self,i):
		arr = [self.Grid[i][y] for y in range(self.dim)]
		if (0 in arr):
			return False
		if sum(arr)==self.turn*self.dim:
			return True
		return False

	def horizontal_win(self,i):
		arr = [self.Grid[x][i] for x in range(self.dim)]
		if (0 in arr):
			return False
		if sum(arr)==self.turn*self.dim:
			return True
		return False
	def diagonal1_win(self):
		arr = [self.Grid[i][i] for i in range(self.dim)]
		if (0 in arr):
			return False
		if sum(arr)==self.turn*self.dim:
			return True
		return False
	def diagonal2_win(self):
		arr = [self.Grid[i][self.dim-1-i] for i in range(self.dim)]
		if (0 in arr):
			return False
		if sum(arr)==self.turn*self.dim:
			return True
		return False

	def on_mouse_press(self,x,y,buttonn,mofi):
		coord = (int(x // self.bwidth),int(y // self.bheight))
		if (self.Grid[coord[0]][coord[1]] != 0):
			return
		if (self.turn == 1):
			sprite = arcade.Sprite('x.png',0.05)
		else:
			sprite = arcade.Sprite('o.png',0.05)
		self.Grid[coord[0]][coord[1]] = self.turn
		sprite.center_x = int(coord[0] * self.bwidth + self.bwidth/2 )
		sprite.center_y = int(coord[1] * self.bheight + self.bheight/2 )
		self.sprites.append(sprite)
		

		print(coord[0],coord[1])

		if self.vertical_win(coord[0]):
			self.gameOver = True
		
		if self.horizontal_win(coord[1]):
			self.gameOver = True
		
		if self.diagonal1_win():
			self.gameOver = True
		
		if self.diagonal2_win():
			self.gameOver = True
		
		if self.gameOver:
			print("Game Over")
		self.changeTurn()
        
	def changeTurn(self):
		self.turn = self.turn%self.players + 1


	def on_draw(self):
		""" Called whenever we need to draw the window. """
		arcade.start_render()

		self.sprites.draw()
        
		for i in range(0,self.bwidth*self.dim,self.bwidth):
			arcade.draw_line(i,0,i,self.bwidth*self.dim,arcade.color.AERO_BLUE)
		for i in range(0,self.bheight*self.dim,self.bheight):            
			arcade.draw_line(0,i,self.bheight*self.dim,i,arcade.color.AERO_BLUE)

	def update(self, delta_time):
		""" Called to update our objects. Happens approximately 60 times per second."""


def main():
	window = MyGame(100, 100, "Drawing Example",3,2)
	#window.setup()
	arcade.run()


main()
