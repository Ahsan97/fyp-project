from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import RetrieveModelMixin
from HEC.api.serializers import UserSerializer, UserRoleSerializer, PaperPresentingFormSerializer, \
    HigherStudiesFormSerializer, PHDDocumentSerializer, PostDoctorateDocumentSerializer, EmailUser, \
    OralDocumentsSerializer, PosterDocumentsSerializer
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from .models import user_Role, PaperPresentationForms, HigherStudiesForms, PHDDocument, PostDoctorateDocuments, Email, \
    OralPaperPresentationDocuments, PosterPaperPresentationDocuments
from .permissions import CommitteMemberAllowed
from rest_framework import generics, mixins
from django.core.mail import send_mail


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class EmailAPIView(mixins.CreateModelMixin, generics.ListAPIView):
    serializer_class = EmailUser

    def get_queryset(self):
        queryset = Email.objects.all()
        return queryset

    def post(self, request, *args, **kwargs):
        subject = request.data['subject']
        message = request.data['message']
        from_email = 'ahsanzubair320@gmail.com'
        email = request.data['email']
        send_mail(subject, message, from_email, [email], **kwargs)
        return self.create(request, *args, **kwargs)

    def get_serializer_context(self, *args, **kwargs):
        return {'request': self.request}


class UserLoginSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, )
    permission_classes = (IsAuthenticated, )


class UserRoleSet(GenericViewSet, RetrieveModelMixin):
    permission_classes = (IsAuthenticated, )
    serializer_class = UserRoleSerializer

    def get_object(self):
        return user_Role.objects.get(user=self.request.user)



class HigherStudiesView(viewsets.ModelViewSet):
    lookup_field = 'pk'
    lookup_url_kwarg = 'form_id'
    serializer_class = HigherStudiesFormSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        queryset = HigherStudiesForms.objects.all()
        return queryset

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class PHDFileView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    # lookup_field = 'pk'
    lookup_url_kwarg = 'form_id'
    serializer_class = PHDDocumentSerializer
    PHDDocument.objects.all()

    def get_object(self):
        form_id = self.kwargs.get(self.lookup_url_kwarg)
        higherstudies_form = HigherStudiesForms.objects.filter(id=form_id).first()
        return PHDDocument.objects.filter(user=higherstudies_form).first()

    def get_queryset(self):
        queryset = PHDDocument.objects.all()
        return queryset

    def perform_create(self, serializer):
        hsf = HigherStudiesForms.objects.get(user=self.request.user)
        serializer.save(user=hsf)


class PostDoctorateFileView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    # lookup_field = 'fk'
    lookup_url_kwarg = 'form_id'
    serializer_class = PostDoctorateDocumentSerializer
    PostDoctorateDocuments.objects.all()

    def get_object(self):
        form_id = self.kwargs.get(self.lookup_url_kwarg)
        higherstudies_form = HigherStudiesForms.objects.filter(id=form_id).first()
        queryset = PostDoctorateDocuments.objects.filter(user=higherstudies_form).first()
        return queryset

    def get_queryset(self):
        queryset = PostDoctorateDocuments.objects.all()
        return queryset

    def perform_create(self, serializer):
        hsf = HigherStudiesForms.objects.get(user=self.request.user)
        serializer.save(user=hsf)


class PaperPresentingView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    lookup_field = 'pk'
    lookup_url_kwarg = 'form_id'
    serializer_class = PaperPresentingFormSerializer

    def get_queryset(self):
        queryset = PaperPresentationForms.objects.all()
        return queryset

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class OralDocumentView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    # lookup_field = 'pk'
    lookup_url_kwarg = 'form_id'
    serializer_class = OralDocumentsSerializer
    OralPaperPresentationDocuments.objects.all()

    def get_object(self):
        form_id = self.kwargs.get(self.lookup_url_kwarg)
        paperpresentation_form = PaperPresentationForms.objects.filter(id=form_id).first()
        queryset = OralPaperPresentationDocuments.objects.filter(user=paperpresentation_form).first()
        return queryset

    def get_queryset(self):
        queryset = OralPaperPresentationDocuments.objects.all()
        return queryset

    def perform_create(self, serializer):
        ppf = PaperPresentationForms.objects.get(user=self.request.user)
        serializer.save(user=ppf)


class PosterDocumentView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    # lookup_field = 'pk'
    lookup_url_kwarg = 'form_id'
    serializer_class = PosterDocumentsSerializer
    PosterPaperPresentationDocuments.objects.all()

    def get_object(self):
        form_id = self.kwargs.get(self.lookup_url_kwarg)
        paperpresentation_form = PaperPresentationForms.objects.filter(id=form_id).first()
        queryset = PosterPaperPresentationDocuments.objects.filter(user=paperpresentation_form).first()
        return queryset

    def get_queryset(self):
        queryset = PosterPaperPresentationDocuments.objects.all()
        return queryset

    def perform_create(self, serializer):
        ppf = PaperPresentationForms.objects.get(user=self.request.user)
        serializer.save(user=ppf)
