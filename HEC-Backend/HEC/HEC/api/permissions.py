from rest_framework.permissions import BasePermission
from .models import user_Role


class CommitteMemberAllowed(BasePermission):
    def has_permission(self, request, view):
        value = user_Role.objects.get(user=request.user)
        return value.user_role == 'committe'




