from django.conf.urls import url, include
from rest_framework import routers

from .views import UserRoleSet, UserViewSet, PaperPresentingView, HigherStudiesView, PHDFileView, PostDoctorateFileView,\
    EmailAPIView, OralDocumentView, PosterDocumentView
from rest_framework.authtoken.views import ObtainAuthToken
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import views as auth_views


router = routers.DefaultRouter()
router.register(r'register', UserViewSet)


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^login/', ObtainAuthToken.as_view()),

    url(r'^paper-presenting-form/$', PaperPresentingView.as_view({'get': 'list', 'post': 'create'})),
    url(r'^paper-presenting-form/(?P<form_id>.+)/', PaperPresentingView.as_view({'get': 'retrieve'})),

    url(r'^higher-studies-form/$', HigherStudiesView.as_view({'get': 'list', 'post': 'create'})),
    url(r'^higher-studies-form/(?P<form_id>.+)/', HigherStudiesView.as_view({'get': 'retrieve'})),

    url(r'higher-studies-phd-documents/$', PHDFileView.as_view({'get': 'list', 'post': 'create'})),
    url(r'^higher-studies-phd-documents/(?P<form_id>.+)/', PHDFileView.as_view({'get': 'retrieve'})),

    url(r'higher-studies-postdoctorate-documents/$', PostDoctorateFileView.as_view({'get': 'list', 'post': 'create'})),
    url(r'^higher-studies-postdoctorate-documents/(?P<form_id>.+)/', PostDoctorateFileView.as_view({'get': 'retrieve'})),

    url(r'paper-presentation-oral-documents/$', OralDocumentView.as_view({'get': 'list', 'post': 'create'})),
    url(r'paper-presentation-oral-documents/(?P<form_id>.+)/', OralDocumentView.as_view({'get': 'retrieve'})),

    url(r'paper-presentation-poster-documents/$', PosterDocumentView.as_view({'get': 'list', 'post': 'create'})),
    url(r'paper-presentation-poster-documents/(?P<form_id>.+)/', PosterDocumentView.as_view({'get': 'retrieve'})),

    url(r'^role/', UserRoleSet.as_view({'get': 'retrieve'})),

    url(r'email-user/', EmailAPIView.as_view()),

    url(r'password-reset/', csrf_exempt(auth_views.PasswordResetView.as_view())),
    url(r'password-reset/done/', csrf_exempt(auth_views.PasswordResetDoneView.as_view())),
    url(r'password-reset-confirm/<uidb64>/<token>/', csrf_exempt(auth_views.PasswordResetConfirmView.as_view())),
    url(r'password_reset_complete/',csrf_exempt(auth_views.PasswordResetCompleteView.as_view()))
]
