# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import user_Role, PaperPresentationForms, HigherStudiesForms, PHDDocument, PostDoctorateDocuments, Email, \
    OralPaperPresentationDocuments, PosterPaperPresentationDocuments

# Register your models here.

admin.site.register(user_Role)
admin.site.register(PaperPresentationForms)
admin.site.register(HigherStudiesForms)
admin.site.register(PHDDocument)
admin.site.register(PostDoctorateDocuments)
admin.site.register(Email)
admin.site.register(OralPaperPresentationDocuments)
admin.site.register(PosterPaperPresentationDocuments)
