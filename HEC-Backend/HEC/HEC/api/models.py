# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User

role_choices = (
    ('applicant', 'Applicant'),
    ('committe', 'Committee Member'),
)
# Create your models here.

travel_purpose_choices = (
    ('1', 'Application for Ph.D'),
    ('2', 'Application for Post-Doctorate'),
)


class user_Role(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    user_role = models.CharField(max_length=20, choices=role_choices, default='applicant')

    def __str__(self):
        return self.user.username


class HigherStudiesForms(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True)
    name = models.CharField(max_length=150, null=False)
    cnic = models.CharField(max_length=15, null=False)
    highest_qualification = models.CharField(max_length=50, null=False)
    designation = models.CharField(max_length=12, null=False)
    department = models.CharField(max_length=50, null=False)
    university = models.CharField(max_length=200, null=False)
    address = models.TextField(default='', null=False)
    cellphone = models.CharField(max_length=11, null=False)
    email = models.EmailField(default='', null=False)
    travel_purpose = models.CharField(max_length=200, null=False)
    country_name = models.CharField(max_length=500, null=False)
    university_name = models.CharField(max_length=500, null=False)
    department_name = models.CharField(max_length=500, null=False)
    program_name = models.CharField(max_length=250, null=False)
    research_title = models.CharField(max_length=500, null=False)
    registration_date = models.DateField()
    joining_date = models.DateField(null=False)
    stay_duration = models.CharField(max_length=150, null=False)
    return_date = models.DateField(null=False)
    travel_cost = models.CharField(max_length=150, null=False)
    travel_sponsor = models.CharField(max_length=250)
    tuition_fee = models.CharField(max_length=200, null=False)
    tuition_fee_sponsor = models.CharField(max_length=250)
    accommodation_cost = models.CharField(max_length=150, null=False)
    accommodation_sponsor = models.CharField(max_length=250)
    other_cost = models.CharField(max_length=150, null=False)
    other_cost_sponsor = models.CharField(max_length=250)
    hec_travel_cost = models.CharField(max_length=250, null=False)

    def __str__(self):
        return self.name


class PHDDocument(models.Model):
    user = models.ForeignKey(HigherStudiesForms, on_delete=models.PROTECT, null=True, blank=True)
    admission_letter = models.FileField(blank=False, null=False)
    award_letter = models.FileField(blank=False, null=False)
    attested_degree = models.FileField(blank=False, null=False)
    cv = models.FileField(blank=False, null=False)
    travel_agent = models.FileField(blank=False, null=False)
    surety_bond = models.FileField(blank=False, null=False)
    attested_surety_bond = models.FileField(blank=False, null=False)
    guarantee_cnic = models.FileField(blank=False, null=False)
    study_leave = models.FileField(blank=False, null=False)
    objection_certificate = models.FileField(blank=False, null=False)
    hod_form = models.FileField(blank=False, null=False)
    rector_form = models.FileField(blank=False, null=False)

    def __str__(self):
        return str(self.user.name)


class PostDoctorateDocuments(models.Model):
    user = models.ForeignKey(HigherStudiesForms, on_delete=models.PROTECT, null=True, blank=True)
    placement_letter = models.FileField(blank=False, null=False)
    research_description = models.FileField(blank=False, null=False)
    cv = models.FileField(blank=False, null=False)
    agent_quotation = models.FileField(blank=False, null=False)
    study_leave = models.FileField(blank=False, null=False)
    no_objection = models.FileField(blank=False, null=False)
    application_sign = models.FileField(blank=False, null=False)
    application_stamped = models.FileField(blank=False, null=False)
    stamped_vc = models.FileField(blank=False, null=False)

    def __str__(self):
        return str(self.user.name)

class PaperPresentationForms(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True)
    status = models.CharField(max_length=150, null=False)
    name = models.CharField(max_length=50, null=False)
    cnic = models.CharField(max_length=16, null=False)
    highest_qualification = models.CharField(max_length=50, null=False)
    designation = models.CharField(max_length=12, null=False)
    department = models.CharField(max_length=50, null=False)
    university = models.CharField(max_length=200, null=False)
    address = models.TextField(default='', null=False)
    cellphone = models.CharField(max_length=11, null=False)
    email = models.EmailField(default='', null=False)
    event_title = models.CharField(max_length=300, null=False)
    event_starting_date = models.DateField(null=False)
    event_ending_date = models.DateField(null=False)
    venue = models.CharField(max_length=200, null=False)
    organizer = models.CharField(max_length=250, null=False)
    website_address = models.URLField(null=False)
    event_type = models.CharField(max_length=40, null=False)
    research_title = models.CharField(max_length=300, null=False)
    paper_status = models.CharField(max_length=40, null=False)
    presentation_mode = models.CharField(max_length=30, null=False)
    paper_review = models.CharField(max_length=10, null=True)
    paper_publish = models.CharField(max_length=50, null=False)
    travel_cost = models.CharField(max_length=50, null=False)
    travel_sponsor = models.CharField(max_length=100)
    registration_cost = models.CharField(max_length=50, null=False)
    registration_sponsor = models.CharField(max_length=100)
    accommodation_cost = models.CharField(max_length=50, null=False)
    accommodation_sponsor = models.CharField(max_length=100)
    daily_allowance_cost = models.CharField(max_length=50, null=False)
    daily_allowance_sponsor = models.CharField(max_length=100, null=False)
    hec_travel_cost = models.CharField(max_length=100, null=True)
    hec_registration_fee = models.CharField(max_length=100, null=True)
    hec_accommodation = models.CharField(max_length=100, null=True)
    hec_allowance = models.CharField(max_length=100, null=True)
    scholarship_details = models.CharField(max_length=10, null=True)
    travel_grant_details = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.name


class OralPaperPresentationDocuments(models.Model):
    user = models.ForeignKey(PaperPresentationForms, on_delete=models.PROTECT,null=True, blank=True)
    acceptance_letter = models.FileField(blank=False, null=False)
    presentation_mode = models.FileField(blank=False, null=False)
    acceptance_peer_review = models.FileField(blank=False, null=False)
    publish_evidence = models.FileField(blank=False, null=False)
    events_brochure = models.FileField(blank=False, null=False)
    full_paper = models.FileField(blank=False, null=False)
    author_noc = models.FileField(blank=False, null=False)
    travel_agent_quotation = models.FileField(blank=False, null=False)
    cv = models.FileField(blank=False, null=False)

    def __str__(self):
        return str(self.user.name)


class PosterPaperPresentationDocuments(models.Model):
    user = models.ForeignKey(PaperPresentationForms, on_delete=models.PROTECT, null=True, blank=True)
    acceptance_letter = models.FileField(blank=False, null=False)
    presentation_mode = models.FileField(blank=False, null=False)
    acceptance_peer_review = models.FileField(blank=False, null=False)
    publish_evidence = models.FileField(blank=False, null=False)
    events_brochure = models.FileField(blank=False, null=False)
    full_paper = models.FileField(blank=False, null=False)
    author_noc = models.FileField(blank=False, null=False)
    travel_agent_quotation = models.FileField(blank=False, null=False)
    cv = models.FileField(blank=False, null=False)
    publication_list = models.FileField(blank=False, null=False)

    def __str__(self):
        return str(self.user.name)


class Email(models.Model):
    email = models.EmailField(null=False)
    subject = models.CharField(max_length=250, null=False)
    message = models.TextField(default='', null=False, blank=False)

    def __str__(self):
        return self.email
