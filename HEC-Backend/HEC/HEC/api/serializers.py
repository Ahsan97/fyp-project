from django.contrib.auth.models import User
from rest_framework import serializers
from .models import user_Role, PaperPresentationForms, HigherStudiesForms, PHDDocument, PostDoctorateDocuments, Email, \
    OralPaperPresentationDocuments, PosterPaperPresentationDocuments
from rest_framework.fields import EmailField


class UserSerializer(serializers.HyperlinkedModelSerializer):
    email = EmailField(required=True, allow_null=False)

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True, 'required': True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        user_Role.objects.create(user=user)
        Subject = 'User Registered'
        Message = 'Thank You for creating your account on HEC Travel Grant System \n' + 'Username: ' + user.username
        user.email_user(subject=Subject, message=Message)
        return user


class EmailUser(serializers.ModelSerializer):
    class Meta:
        model = Email
        fields = ('id', 'email', 'subject', 'message')


class UserRoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_Role
        fields = ('id', 'user', 'user_role')
        read_only_fields = ('id', 'user', 'user_role')


class PaperPresentingFormSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaperPresentationForms
        fields = ('id', 'status', 'name', 'cnic', 'highest_qualification', 'designation', 'department', 'university',
                  'address', 'cellphone', 'email', 'event_title', 'event_starting_date', 'event_ending_date', 'venue',
                  'organizer', 'website_address', 'event_type', 'research_title', 'paper_status', 'presentation_mode',
                  'paper_review', 'paper_publish', 'travel_cost', 'travel_sponsor', 'registration_cost',
                  'registration_sponsor', 'accommodation_cost', 'accommodation_sponsor', 'daily_allowance_cost',
                  'daily_allowance_sponsor', 'hec_travel_cost', 'hec_registration_fee', 'hec_accommodation',
                  'hec_allowance', 'scholarship_details', 'travel_grant_details')


class HigherStudiesFormSerializer(serializers.ModelSerializer):
    class Meta:
        model = HigherStudiesForms
        fields = ('id', 'name', 'cnic', 'highest_qualification', 'designation', 'department', 'university',
                  'address', 'cellphone', 'email', 'travel_purpose', 'country_name', 'university_name',
                  'department_name', 'program_name', 'research_title', 'registration_date', 'joining_date',
                  'stay_duration', 'return_date', 'travel_cost', 'travel_sponsor', 'tuition_fee', 'tuition_fee_sponsor',
                  'accommodation_cost', 'accommodation_sponsor', 'other_cost', 'other_cost_sponsor', 'hec_travel_cost')


class PHDDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = PHDDocument
        fields = ('id', 'admission_letter', 'award_letter', 'attested_degree', 'cv', 'travel_agent', 'surety_bond',
                  'attested_surety_bond', 'guarantee_cnic', 'study_leave', 'objection_certificate', 'hod_form',
                  'rector_form')



class PostDoctorateDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostDoctorateDocuments
        fields = ('id', 'placement_letter', 'research_description', 'cv', 'agent_quotation', 'study_leave',
                  'no_objection', 'application_sign', 'application_stamped')


class OralDocumentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OralPaperPresentationDocuments
        fields = ('id', 'acceptance_letter', 'presentation_mode', 'acceptance_peer_review', 'publish_evidence',
                  'events_brochure', 'full_paper', 'author_noc', 'travel_agent_quotation', 'cv')


class PosterDocumentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = PosterPaperPresentationDocuments
        fields = ('id', 'acceptance_letter', 'presentation_mode', 'acceptance_peer_review', 'publish_evidence',
                  'events_brochure', 'full_paper', 'author_noc', 'travel_agent_quotation', 'cv', 'publication_list')
